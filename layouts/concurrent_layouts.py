##########
## Fichier de rendu des graphes de concurrences
##########

import pandas as pd
import numpy as np
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objs as go

############
## Extraction des données
############

datas = pd.read_csv('Data/gucci-concurrent.csv',sep=';')
evo = pd.read_csv('Data/evo-concurrent.csv',sep=';')
datas = np.array(datas)
res = []
for i in datas:
    res += [i[1:]]

res = [['Giorgio Armani','Louis Vuitton','Dolce & Gabana','Prada','Dior','Gucci']] + res

evo_dict = {
    'dates': evo['marque'].values[:10],
    'Giorgio Armani': evo['Giorgio Armani'].values[:10],
    'Louis Vuitton': evo['Louis Vuitton'].values[:10],
    'Dolce & Gabana': evo['Dolce & Gabana'].values[:10],
    'Prada': evo['Prada'].values[:10],
    'Dior': evo['Dior '].values[:10],
    'Gucci': evo['Gucci'].values[:10],
}

############
## Graphique Evolution du nombre d'abonnés
############

def render_evolution():
    return( dcc.Graph(
        id = 'graph0',
        figure = {
            'data': [
                go.Scatter(
                    x = evo_dict['dates'],
                    y = evo_dict['Giorgio Armani'],
                    mode = 'lines',
                    name= 'Giorgio Armani'
                ),
                go.Scatter(
                    x = evo_dict['dates'],
                    y = evo_dict['Louis Vuitton'],
                    mode = 'lines',
                    name= 'Louis Vuitton'
                ),
                go.Scatter(
                    x = evo_dict['dates'],
                    y = evo_dict['Dolce & Gabana'],
                    mode = 'lines',
                    name= 'Dolce & Gabana'
                ),
                go.Scatter(
                    x = evo_dict['dates'],
                    y = evo_dict['Prada'],
                    mode = 'lines',
                    name= 'Prada'
                ),
                go.Scatter(
                    x = evo_dict['dates'],
                    y = evo_dict['Dior'],
                    mode = 'lines',
                    name= 'Dior'
                ),
                go.Scatter(
                    x = evo_dict['dates'],
                    y = evo_dict['Gucci'],
                    mode = 'lines',
                    name= 'Gucci'
                ),
            ],
            'layout' : {
                'title' :'Évolution du nombre d\'abonnés sur Facebook',
                'width':800,
            }
        }
    ))

############
## Graphique likes facebook
############

def render_like_facebook():
    return dcc.Graph(
        id='graph1',
        figure={
            'data': [
                {'x':res[0], 'y':res[1], 'type': 'bar','name':'Likes'}
            ],
            'layout': {
                'title': 'Nombre de likes sur facebook',
                'width':450
            }
        }
    )

############
## Graphique abonnés facebook
############

def render_follower_facebook():
    return dcc.Graph(
        id='graph2',
        figure={
            'data': [
                {'x':res[0], 'y':res[2], 'type': 'bar','name':'Likes'}
            ],
            'layout': {
                'title': 'Nombre d\'abonnées sur Facebook',
                'width':450
            }
        }
    )

############
## Graphique Follower Twitter
############

def render_follower_twitter():
    return dcc.Graph(
        id='graph3',
        figure={
            'data': [
                {'x':res[0], 'y':res[3], 'type': 'bar','name':'Likes'}
            ],
            'layout': {
                'title': 'Nombre de followers sur Twitter',
                'width':450
            }
        }
    )

############
## Graphique nombre de tweets
############

def render_nombre_tweets():
    return dcc.Graph(
        id='graph4',
        figure={
            'data': [
                {'x':res[0], 'y':res[4], 'type': 'bar','name':'Likes'}
            ],
            'layout': {
                'title': 'Nombre de tweets sur Twitter',
                'width':450
            }
        }
    )

############
## Rendu du layout component
############

def concurrence_layout():
    return [
        html.Div(render_evolution(),className='row_twitter_1'),
        html.Div(
            [render_like_facebook(),render_follower_facebook()],
            className='row_twitter_1'
        ),
        html.Div(
            [render_follower_twitter(),render_nombre_tweets()],
            className="row_twitter_1"
        )
    ]
