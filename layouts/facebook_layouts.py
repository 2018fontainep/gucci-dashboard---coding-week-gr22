############
## Fichier rendu du layout facebook
############

import dash_html_components as html
import dash_core_components as dcc
from layouts.components_layouts import small_square
import plotly.graph_objs as go
from matplotlib_integration import fig_to_uri
import matplotlib.pyplot as plt
import os
from os import path
from sentiment_analysis import facebook_data
from wordcloud_generator import wordcloud_facebook_uri
from sentiment_emoticons_render import sentiment_emoticons_render

############
## Rendu du diagramme camembert de sentiment
############

def render_sentiment():
    pos, neut, neg = facebook_data()
    return dcc.Graph(
        id='sentiment_graph',
        figure = {
            'data' : [
                go.Pie(
                    labels= ['Positif','Neutre','Négatif'],
                    values= [pos,neut,neg],
                    marker= dict(colors=['#4bf442','#bababa','#b50e0e'])
                ),
            ],

            'layout' : {
            'title':'Sentiment sur Facebook',
            'width': 535
            }
        }
    )

############
## Récupération URI du WordCloud
############

uri = wordcloud_facebook_uri()

############
## Layout Facebook
############

def facebook_layout():
    return ([
        html.Div([
            small_square("Nombre d'abonnés",'5.06M','1','1%','followers'),
            small_square("Nombre de partages",'350K','1','1.2%','shares'),
            small_square("Nombre de likes",'1.2M','1','12%','likes'),
            small_square("Nombre de mentions",'65K','1','4.5%','mentions')
        ],className='row-stats'),
        html.Div([
            render_sentiment(),html.Img(src=uri, style={'marginTop':30,'width':535}, className="wordcloud_block")
        ],className='row_twitter_1'),
        html.Div([
            sentiment_emoticons_render('facebook')
        ],className='row_twitter_1')]

    )
