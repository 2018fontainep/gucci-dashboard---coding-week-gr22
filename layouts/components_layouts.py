############
## Fichier Composant HTML
############

import dash_html_components as html
import dash_core_components as dcc

############
## Carré statistiques Twitter et Facebook
############

def small_square(title,number,direction,evolution,id_name):
    link = ''
    evo=''
    if direction == 0:
        link = 'https://image.ibb.co/do6FkL/down-arrow.png'
        evo = 'down'
    else:
        link = 'https://i.imgur.com/4Uvl9Ye.png',
        evo = 'up'
    return(
        html.Div(
            [
                html.Div(title,className='title'),
                dcc.Dropdown(
                    id=id_name+'-dropdown',
                    options=[
                                {'label': '7 jours', 'value': 0},
                                {'label': '3 jours', 'value': 1},
                                {'label': '1 jours', 'value': 2}
                            ],
                        value=2
                        ),
                html.Div([
                    html.Div([
                        html.Div(
                        [
                            html.Div(number,className="absolute",id=id_name),
                            html.Div([
                                html.Img(src=link),
                                evolution
                            ],className=evo),
                        ],className="number"),
                        html.Div(
                            html.Img(src='https://i.imgur.com/ndYNd4n.png'),
                            className='image'
                        )
                    ],className='numbers')
                ],className='content')
            ],className='small_square')
    )
