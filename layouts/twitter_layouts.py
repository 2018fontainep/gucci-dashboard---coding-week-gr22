############
## Fichier de rendu du layout twitter
############

import dash_html_components as html
import dash_core_components as dcc
from layouts.components_layouts import small_square
import plotly.graph_objs as go
from sentiment_analysis import sentiment_data
from wordcloud_generator import wordcloud_twitter_uri
from sentiment_emoticons_render import sentiment_emoticons_render
from tweets_prediction import predict_tweet

############
## Icons analyse de tweets
############

SENTIMENT_ICONS = ['https://i.imgur.com/SuoRna1.png','https://i.imgur.com/bU7ae9S.png','https://i.imgur.com/IHikMsv.png']

def render_sentiment():
    pos, neut, neg = sentiment_data()
    return dcc.Graph(
        id='sentiment_graph',
        figure = {
            'data' : [
                go.Pie(
                    labels= ['Positif','Neutre','Négatif'],
                    values= [pos,neut,neg],
                    marker= dict(colors=['#4bf442','#bababa','#b50e0e'])
                ),
            ],

            'layout' : {
            'title':'Sentiment sur Twitter',
            'width': 400
            }
        }
    )

############
## Récupération uri nuage de mots
############

uri = wordcloud_twitter_uri()

############
## Layout Twitter
############

def twitter_layout():
    return(
    [
    html.Div([
        small_square("Nombre d'abonnés",'5.06M','1','1%','followers'),
        small_square("Nombre de retweets",'350K','1','1.2%','shares'),
        small_square("Nombre de likes",'1.2M','1','12%','likes'),
        small_square("Nombre de mentions",'65K','1','4.5%','mentions')
    ],className='row-stats'),
    html.Div([dcc.Graph(
            id='example-graph',
            figure={
                'data': [
                    go.Scatter(
                x=[str(i)+' jours' for i in range(1,31)].reverse(),
                y=[5570281,5570562,5569439,5569824,5570092,5570466,5571728,5572149,5572673,5572996,5573567,5573186,5573649,5574184,5568417,5564681,5565003,5558518,5556406,5526631,5524957,5524961,5525199,5525537,5525547,5525352,5525749,5525841,5526219,5526348],
                fill='tozeroy')
                ],
                'layout': {
                    'width':535,
                    'autosizable':True,
                    'title': 'Evolution des followers',
                    'xaxis' : dict(
                        title = "Il y a ( en jours )",
                        autorange=True),
                    'yaxis' : dict(
                        title = "Nombres de followers",
                        range=[5446348,5650281]
                    )
                }
            }
        ),render_sentiment()],className="row_twitter_1"),
        html.Div([
            html.Img(src=uri, style={'marginTop':30,'width':535}, className="wordcloud_block"),
            html.Div([
                'Prédiction de la réussite d\'un tweet',
                dcc.Textarea(
                placeholder='Entrer un tweet...',
                value = '',
                id='tweet_prediction_placeholder'
                ),
                html.Button('Tester',id='tweet_prediction'),
                html.Img(src=SENTIMENT_ICONS[2], id='tweet_prediction_img')
            ],className="prediction_block")
        ],className="row_twitter_1"),
        html.Div([
            sentiment_emoticons_render('twitter')
        ],className="row_twitter_1")
        ]
    )
