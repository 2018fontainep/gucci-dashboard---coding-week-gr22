##########
## Importation des modules
##########

import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input,Output,State
import plotly.plotly as py
import plotly.graph_objs as go
from layouts.components_layouts import small_square
from layouts.facebook_layouts import facebook_layout
from layouts.twitter_layouts import twitter_layout, render_sentiment
from tweets_prediction import predict_tweet
from layouts.concurrent_layouts import concurrence_layout
import pandas as pd
import numpy as np

from datetime import datetime
import pandas_datareader.data as web

##########
## Importations des variables utiles
##########

app = dash.Dash()
app.config['suppress_callback_exceptions']=True

SENTIMENTS_EMOTS = {
    'anger' : 'https://i.imgur.com/2Xl7t01.png',
    'disgust' : 'https://i.imgur.com/bY2XcXS.png',
    'fear' : 'https://i.imgur.com/v3aLLbq.png',
    'joy' : 'https://i.imgur.com/vo8SQoH.png',
    'sadness' : 'https://i.imgur.com/efuHAnB.png',
    'surprise' : 'https://i.imgur.com/3OC4oQ7.png'
}

SENTIMENT_ICONS = ['https://i.imgur.com/SuoRna1.png','https://i.imgur.com/bU7ae9S.png','https://i.imgur.com/IHikMsv.png']

FACEBOOK_LAYOUT = facebook_layout()
TWITTER_LAYOUT = twitter_layout()

CONCURRENCE_LAYOUT= concurrence_layout()

facebook_datas = np.array(pd.read_csv('Data/facebook_accounts_data.csv',sep=';'))
twitter_datas = np.array(pd.read_csv('Data/twitter_accounts_data.csv',sep=';'))

FOLLOWERS_FACEBOOK = facebook_datas[0]
SHARES_FACEBOOK = facebook_datas[1]
MENTIONS_FACEBOOK = facebook_datas[2]
LIKES_FACEBOOK = facebook_datas[3]

FOLLOWERS_TWITTER = twitter_datas[0]
SHARES_TWITTER = twitter_datas[1]
MENTIONS_TWITTER = twitter_datas[2]
LIKES_TWITTER = twitter_datas[3]


##########
## Layout Principal ( Menu et container )
##########

app.layout = html.Div(style={'backgroundColor':'#F0F0F7'},children=[

    html.Div(className='menu-container', children=[

        html.Div(
            [
            html.Span('G',style={'font-size':'46px','font-weight':'700'}),
            'Cruscotto'
            ]
        ,className='menu-header'),

        html.Ul(className='menu', children=[
            html.Li([
                html.Span(
                    html.Img(src='https://i.imgur.com/HyyyHcP.png')
                ),
                html.Span('Twitter')
            ],className='menu-item',id='menu-twitter',n_clicks_timestamp=0),html.Li([
                html.Span(
                    html.Img(src='https://i.imgur.com/Ori9KDY.png')
                ),
                html.Span('Facebook')
            ],className='menu-item',id='menu-facebook',n_clicks_timestamp=0),
            html.Li([
                html.Span(
                    html.Img(src='https://i.imgur.com/oTxmNbD.png',style={'width':17,'height':9})
                ),
                html.Span('Concurrence')
            ],className='menu-item',id='menu-concurrence',n_clicks_timestamp=0)
        ])
    ]),

    html.Div(
        FACEBOOK_LAYOUT
    ,className='main-container',id='page-content'),
    html.Script(src='static/app.js'),
    html.Link(href='static/app.css',rel='stylesheet'),
    html.Link(href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700', rel="stylesheet")
])

##########
## Callbacks
##########

## Callback du menu

@app.callback(
    Output(component_id='page-content',component_property='children'),
    [Input('menu-twitter','n_clicks_timestamp'),Input('menu-facebook','n_clicks_timestamp'),Input('menu-concurrence','n_clicks_timestamp')])
def display_data(twitter,facebook,concurrence):
    global FACEBOOK_LAYOUT
    global TWITTER_LAYOUT
    global CONCURRENCE_LAYOUT
    if(int(facebook)>int(twitter) and int(facebook)> int(concurrence)):
        return FACEBOOK_LAYOUT
    elif(int(twitter > int(concurrence))):
        return TWITTER_LAYOUT
    else:
        return CONCURRENCE_LAYOUT

## Callback des modules abonnés / likes etc...

@app.callback(
    Output(component_id='followers', component_property='children'),
    [Input(component_id='followers-dropdown',component_property='value')]
)
def change_data(val):
    global FOLLOWERS_FACEBOOK
    return FOLLOWERS_FACEBOOK[val]

@app.callback(
    Output(component_id='shares',component_property='children'),
    [Input(component_id='shares-dropdown',component_property='value')]
)
def change_data(val):
    global FOLLOWERS_FACEBOOK
    return FOLLOWERS_FACEBOOK[val]

@app.callback(
    Output(component_id='likes',component_property='children'),
    [Input(component_id='likes-dropdown',component_property='value')]
)
def change_data(val):
    global FOLLOWERS_FACEBOOK
    return FOLLOWERS_FACEBOOK[val]

@app.callback(
    Output(component_id='mentions',component_property='children'),
    [Input(component_id='likes-dropdown',component_property='value')]
)
def change_data(val):
    global FOLLOWERS_FACEBOOK
    return FOLLOWERS_FACEBOOK[val]

## Callback prédiction des tweets

@app.callback(
    Output(component_id='tweet_prediction_img',component_property='src'),
    [Input('tweet_prediction','n_clicks')],
    [State('tweet_prediction_placeholder','value')]
)
def update_output(n_clicks, value):
    global SENTIMENT_ICONS
    res = predict_tweet(value)
    if res <= -100 :
        return SENTIMENT_ICONS[1]
    elif res >= 300:
        return SENTIMENT_ICONS[0]
    else:
        return SENTIMENT_ICONS[2]

##########
## Lancement du serveur
##########

if __name__ == "__main__":
    app.config['suppress_callback_exceptions']=True
    app.run_server(debug=True)
