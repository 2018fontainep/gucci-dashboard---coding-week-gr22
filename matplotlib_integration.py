############
## Intégration d'un diagramme matplotlib dans DASH
## ( On passe par la génération d'une image en Bytes64 )
############

import matplotlib.pyplot as plt
import numpy as np
from io import BytesIO
import base64

def fig_to_uri(in_fig, close_all=True, **save_args):

    out_img = BytesIO()
    in_fig.savefig(out_img, format='png', **save_args)
    if close_all:
        in_fig.clf()
        plt.close('all')
    out_img.seek(0)
    encoded = base64.b64encode(out_img.read()).decode('ascii').replace("\n",'')
    return "data:image/png;base64,{}".format(encoded)
