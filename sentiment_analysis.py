import pandas as pd
############
## Analyse de Sentiment twitter
############

def sentiment_data():
    tweets = pd.read_csv('Data/twitter_sentiment.csv',delimiter=";")
    positive = 0
    negative = 0
    neutral = 0
    for sentiment in tweets['sentiment']:
        if sentiment == 'positive':
            positive += 1
        elif sentiment == 'neutral':
            neutral += 1
        else:
            negative += 1

    n = len(tweets.index)
    return(positive/n,neutral/n,negative/n)

############
## Extraction chaine de caractère pour wordclouds
############

def text_data():
    tweets = pd.read_csv('Data/twitter_sentiment.csv',delimiter=';')
    str = ''
    for t in tweets['text']:
        str += t
    return str

############
## Analyse de sentiment facebook
############

def facebook_data():
    comments = pd.read_csv('Data/facebook-final.csv',delimiter=';')
    positive = 0
    negative = 0
    neutral = 0
    for sentiment in comments['sentiment']:
        if sentiment == 'positive':
            positive += 1
        elif sentiment == 'neutral':
            neutral+=1
        else:
            negative += 1
    n = len(comments.index)
    return(positive/n, neutral/n, negative /n)

############
## Génération chaine pour wordcloud facebook
############

def text_facebook():
    comments = pd.read_csv('Data/facebook-final.csv',delimiter=';')
    str = ''
    for c in comments['text']:
        str += c
    return str
