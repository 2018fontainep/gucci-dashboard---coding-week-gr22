import pandas
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn.linear_model import RidgeClassifier
from sklearn.linear_model import LinearRegression

############
## Module de prédiction des tweets
############

datas = pandas.read_csv('Data/gucci-account-prediction.csv', delimiter=';')
datas['score'] = datas['retweets']+datas['likes']
datas['text'].str.lower()
datas['text'].replace('[^a-zA-Z0-9]',' ', regex=True)
datas = datas.reset_index()

import re

############
## Preprocessing
############
def tokenizer(text):
    if text:
        result = re.findall('[a-z]{2,}',text.lower())
    else:
        result = []
    return result

X = datas['text'].values
y = datas['score'].values.astype('int')

############
## Model fitting
############

import numpy as np
vect = TfidfVectorizer(tokenizer=tokenizer, stop_words='english', dtype=np.float32)
X_train = vect.fit_transform(X)

r = RidgeClassifier(alpha=0.01,max_iter = 10000).fit(X_train, y)

############
## Prédiction
############

def predict_tweet(text):
    text = vect.transform([text])
    return(r.predict(text)[0])
