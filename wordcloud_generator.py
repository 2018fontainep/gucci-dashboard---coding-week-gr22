import matplotlib.pyplot as plt

from wordcloud import WordCloud
from matplotlib_integration import fig_to_uri
from sentiment_analysis import text_data
from matplotlib_integration import fig_to_uri
from sentiment_analysis import text_facebook
import numpy as np
from PIL import Image

############
## Générateur de l'url et suppression des tweets qui semblent hors sujets
############

def wordcloud_twitter_uri():
    str = text_facebook()
    str = str.replace('https','')
    str = str.replace('butt','')
    str = str.replace('mane','')
    wave_mask = np.array(Image.open("img/re2.jpg"))
    wordcloud = WordCloud(max_font_size=400,min_font_size=5,mask=wave_mask,background_color='white').generate(str)

    fig = plt.figure()
    ax1 = fig.add_subplot(1,1,1)
    plt.axis('off')
    plt.imshow(wordcloud)
    uri = fig_to_uri(fig)
    return uri

def wordcloud_facebook_uri():
    str = text_facebook()
    str = str.replace('https','')
    str = str.replace('butt','')
    str = str.replace('mane','')
    wave_mask = np.array(Image.open("img/re2.jpg"))
    wordcloud = WordCloud(max_font_size=400,min_font_size=5,mask=wave_mask,background_color='white').generate(str)

    fig = plt.figure()
    ax1 = fig.add_subplot(1,1,1)
    plt.axis('off')
    plt.imshow(wordcloud)
    uri = fig_to_uri(fig)
    return uri
