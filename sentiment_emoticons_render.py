import pandas as pd
import dash_html_components as html

############
## Analyse des émotions et génération du module
############

def sentiment_emoticons_render(network):
    emots = {
        'anger' : 'https://i.imgur.com/2Xl7t01.png',
        'disgust' : 'https://i.imgur.com/bY2XcXS.png',
        'fear' : 'https://i.imgur.com/v3aLLbq.png',
        'joy' : 'https://i.imgur.com/vo8SQoH.png',
        'sadness' : 'https://i.imgur.com/efuHAnB.png',
        'surprise' : 'https://i.imgur.com/3OC4oQ7.png'
    }

    if network == 'twitter':
        datas = pd.read_csv('Data/twitter_sentiment_analysis.csv')
    else:
        datas = pd.read_csv('Data/facebook_sentiment_analysis.csv')

    anger,disgust,fear,joy,sadness,surprise = 0,0,0,0,0,0

    for d in datas['emotion']:
        if d == 'anger': anger += 1
        elif d == 'disgust' : disgust += 1
        elif d == 'fear' : fear += 1
        elif d == 'joy' : joy += 1
        elif d == 'sadness' : sadness += 1
        elif d == 'surprise': surprise += 1

    n = len(datas[datas['emotion'] != 'unknown'].index)

    ############
    ## Layout du module
    ############

    return (
        html.Div([
            html.Img(src=emots['anger'],style={'width':20 +240*(anger/n),'height':20 +240*(anger/n)}),
            html.Img(src=emots['disgust'],style={'width':20+ 240*(disgust/n),'height':20 +240*(disgust/n)}),
            html.Img(src=emots['fear'],style={'width':20+240*(fear/n),'height':20 +240*(fear/n)}),
            html.Img(src=emots['joy'],style={'width':20+240*(joy/n),'height':20 +240*(joy/n)}),
            html.Img(src=emots['sadness'],style={'width':20+240*(sadness/n),'height':20 +240*(sadness/n)}),
            html.Img(src=emots['surprise'],style={'width':20+240*(surprise/n),'height':20 +240*(surprise/n)}),
        ], className="sentiment_module")
     )
